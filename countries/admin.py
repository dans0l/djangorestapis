from django.contrib import admin
from countries.models import (Continent, Country, Cities, API, Subscription)

# Register your models here.
admin.site.register(API)
admin.site.register(Subscription)

admin.site.register(Continent)
admin.site.register(Country)
admin.site.register(Cities)
