# Development of Restful A.P.I.s in Django

## Quick Guide
Below are the steps on how to get the web app up and running

1.	Clone it:
```bash
    git clone https://bitbucket.org/dans0l/djangorestapis.git
```
2.	Cd into it:
```bash
    cd djangorestapis
```
3.	Create a venv
```bash
    python3 -m venv venv
```
4.	Activate venv:
```bash
    Mac/Linux: source venv/bin/activate
    Windows: venv\Scripts\activate
```
5.	Install the requirements
```bash
    pip install -r requirements.txt
```
6.	Create DB
```bash
    python manage.py makemigrations
```
7.	Apply DB Changes
```bash
    python manage.py migrate
```
8.	Create a super user by typing the command
```bash
    python manage.py createsuperuser
```
    - Enter the username
    - Enter the email
    - Enter the password
    - Remember these superuser credentials, they will be need when logging into the Django admin interface
9.	Run the server:
    - python manage.py runserver
14.	Navigate to your [localhost](http://127.0.0.1:8000/admin) site
15.	Follow the instructions [here](https://developers.decoded.africa/index.php/2021/07/26/how-to-develop-restful-a-p-i-s-in-django/) on how to initialize and test the system
